var mongoose = require('libs/mongoose');
var async = require('async');
var User = require('models/user');

async.series([
	open,
	dropDb,
	createUsers,
	close
], function(err, res){
	console.log(arguments)
})

function open(callback) {
	mongoose.connection.on('open', callback);
}

function dropDb(callback) {
	var db = mongoose.connection.db;
	db.dropDatabase(callback)
}

function createUsers(callback) {
	var users = [
		{
			username: 'vasa',
			password: 'vasa'
		},
		{
			username: 'peta',
			password: 'peta'
		},
		{
			username: 'admin',
			password: 'admin'
		}
	]
	async.each(users, function(userData, callback){
		var user = new User(userData);
		user.save(callback);
	}, callback)
}

function close(callback) {
	mongoose.disconnect(callback);
}

