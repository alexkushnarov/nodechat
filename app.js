var express = require('express');
var http = require('http');
var path = require('path');
var config = require('api/config');
var log = require('api/libs/log')(module);
var engine = require('ejs-locals');
var HttpError = require('api/error').HttpError;
var events = require('events');
var eventEmitter = new events.EventEmitter();
var app = express();

app.set('eventEmitter', eventEmitter);


app.engine('ejs', engine);

app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');

app.use(express.favicon());
if(app.get('env') == 'development'){
	app.use(express.logger('dev'));
} else {
	app.use(express.logger('default'));
}
app.use(express.bodyParser());
// app.use(express.methodOverride());
app.use(express.cookieParser());

var sessionStore = require('api/libs/sessionStore');

app.use(express.session({
	secret: config.get('session:secret'),
	key: config.get('session:key'),
	cookie: config.get('session:cookie'),
	store: sessionStore
}));

app.use(require('api/middleware/sendHttpError'));

app.use(require('api/middleware/loadUser'));

app.use(app.router);

require('api/routes')(app);

app.use(express.static(path.join(__dirname, 'public')));

app.use(function(err, req, res, next) {
	if(typeof err == 'number') {
		err = new HttpError(err);
	}

	if(err instanceof HttpError) {
		res.sendHttpError(err);
	} else {
		if(app.get('env') == 'development') {
			var errorHandler = express.errorHandler();
			errorHandler(err, req, res, next);
		} else {
			log.error(err);
			err = new HttpError(500);
			res.sendHttpError(err);
		}
	}
})

var server = http.createServer(app);

server.listen(config.get('port'), function(){
  log.info('Express server listening on port ' + config.get('port'));
});

var io = require("./api/socket")(server, app);
