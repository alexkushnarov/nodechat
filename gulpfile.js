'use strict';
var gulp = require('gulp');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var sourcemaps = require('gulp-sourcemaps');
var debug = require('gulp-debug');
var clean = require('gulp-clean');
var gulpIf = require('gulp-if');
var server = require('gulp-develop-server');
var isDevelopment = !process.env.NODE_ENV || process.env.NODE_ENV == 'development';

gulp.task('styles', function(callback) {
	return gulp.src('assets/styles/app.scss')
		.pipe(gulpIf(isDevelopment,sourcemaps.init()))
		.pipe(gulpIf(isDevelopment,sass.sync().on('error', sass.logError),sass({outputStyle: 'compressed'}).on('error', sass.logError)))
		.pipe(gulpIf(isDevelopment,sourcemaps.write()))
		.pipe(gulp.dest('public/css'));
})
gulp.task('js', function(callback) {
	return gulp.src('assets/js/**/*.js')
		.pipe(gulp.dest('public/js'));
})

gulp.task('cleanCompiledStyles', function () {
	return gulp.src('public/css/**/*.*', {read: false})
		.pipe(clean());
});

gulp.task('styles:watch', function () {
  gulp.watch('assets/styles/**/*.scss', gulp.series('styles'));
});

gulp.task('js:watch', function () {
  gulp.watch('assets/js/**/*.js', gulp.series('js'));
});

// run server 
gulp.task( 'server:start', function() {
    server.listen( { 
    	path: './app.js',
    	env: {
    		NODE_ENV: 'development',
    		NODE_PATH: '.'
    	},
    	args :['--debug']
    } );
});
 
// restart server if app.js changed 
gulp.task( 'server:restart', function() {
    gulp.watch(['./app.js','api/**/*.*','views/**/*.*'], server.restart );
});

gulp.task('watchers', gulp.parallel(
	'styles:watch',
	'js:watch',
	'server:restart',
	'server:start'
	)
)
gulp.task('start', gulp.series(
	'cleanCompiledStyles',
	'styles',
	'js',
	'watchers'
	)
)
