var User = require('api/models/user');
var async = require('async');
exports.get = function(req, res){
	res.render('registration')
}
exports.post = function(req, res, next){
	var username = req.body.username;
	var password = req.body.password;
	var firstName = req.body.firstName;
	var lastName = req.body.lastName;
	var email = req.body.email;

	var userData = {
		username: username,
		password: password,
		firstName: firstName,
		lastName: lastName,
		email: email
	}
	async.waterfall([
		function(callback) {
			var user = new User(userData);
			user.save(function(err){
				callback(err, user);
			})
		},
		function(user, callback) {
			User.authorize(user.username, user._plainPassword, callback);
		}
	],
	function(err, user) {
		if(err) {
			if(err.code == 11000) {
				return res.send(403,{message:'such suername or email has already existed'} )
			} else {
				return next(err);
			}
		}
		req.session.user = user._id;
		res.send(200, {});
		
	})
	
}