// var User = require('api/models/user').User;
// var HttpError = require('api/error').HttpError;
// var ObjectID = require('mongodb').ObjectID;
var checkAuth = require('api/middleware/checkAuth')
module.exports = function(app) {
	app.get('/',require('./main').get);

	app.get('/login',require('./login').get);

	app.post('/login',require('./login').post);

	app.post('/logout',require('./logout').post);

	app.get('/registration',require('./registration').get);

	app.post('/registration',require('./registration').post);

	app.get('/chat', checkAuth,require('./chat').get);


	// app.get('/users', function(req, res, next) {
	// 	User.find({}, function(err, users) {
	// 		if(err) return next(err);
	// 		res.json(users);
	// 	})
	// })

	// app.get('/user/:id', function(req, res, next) {
	// 	try {
	// 		var id = new ObjectID(req.params.id);
	// 	} catch (e) {
	// 		return next(new HttpError(404, "User Not Found"));
	// 	}
	//   	User.findById(id, function(err, user) {
	//   		if(err) return next(err);
	//   		if(!user) {
	//   			next(new HttpError(404, "User Not Found"));
	//   		}
	//   		res.json(user);
	//   	})
	// })
}