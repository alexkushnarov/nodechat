exports.post = function(req, res, next) {
	var sid = req.session.id;
	var eventEmitter = req.app.get('eventEmitter');
	req.session.destroy(function(err) {
		eventEmitter.emit("session:reload", sid);
		if(err){
			return next(err);
		}
		res.redirect('/');
	});
}