var log = require('api/libs/log')(module);
var config = require('api/config');
var async = require('async');
var connect = require('connect');
var cookie = require('cookie');
var sessionStore = require('api/libs/sessionStore');
var HttpError = require('api/error').HttpError;
var User = require('api/models/user');

function loadSession(sid, callback) {
	sessionStore.load(sid, function(err, session) {
		if(arguments.length == 0){
			return callback(true, null);
		} else {
			return callback(null, session);
		}
	})
}

function loadUser(session, callback) {
	if(!session.user) {
		log.debug("session %s is anonymous", session.id);
		return callback(true, null);
	}
	log.debug("retrieving user", session.user);

	User.findById(session.user, function(err, user) {
		if(err) {
			return callback(err);
		}
		if(!user) {
			return callback(null, null);
		}
		log.debug("user findById res: " + user);
		callback(null, user);
	})
}

module.exports = function(server, app) {
	var io = require('socket.io')(server);

	io.set('logger', log);

	io.set('authorization', function (request, callback) {

	  async.waterfall([
	  	function(cb){
	  		request.cookies = cookie.parse(request.headers.cookie || '');
	  		var sidCookie = request.cookies[config.get('session:key')];
	  		var sid = connect.utils.parseSignedCookie(sidCookie, config.get('session:secret'));
	  		loadSession(sid, cb);
	  	},
	  	function(session, cb) {
	  		if(!session) {
	  			cb(new HttpError(401, 'no session!'));
	  		}
	  		request.session = session;
	  		loadUser(session, cb);
	  	},
	  	function(user, cb) {
	  		if(!user) {
	  			cb(new HttpError(403, 'Anonymus session!'));
	  		}
	  		request.user = user;
	  		cb(null);
	  	}
	  ], function(err){
	  		if(!err) {
	  			return callback(null, true);
	  		}
	  		if(err instanceof HttpError) {
	  			return callback(null, false);
	  		}
	  		callback(err);
	  });
	});

	var eventEmitter = app.get('eventEmitter');
	eventEmitter.on('session:reload', function(sid) {
		var clients = io.sockets.clients().connected;
		//io.sockets.clients().connected['/#RmwkQCizfFNj23k0AAAA'].request.session
		for(var key in clients) {
			if(clients[key].request.session.id === sid) {
				clients[key].request.connection.end();
			}
		}
	})

	io.sockets.on('connection', function(socket){

		var username = socket.request.user.get('username');
		socket.broadcast.emit('join', username);

	  	socket.on('message', function(data, callback){
	  		socket.broadcast.emit('message', username, data);
	  		callback && callback();
	  	});

	  	socket.on('disconnect', function(){
	  		socket.broadcast.emit('leave', username);
	  	});
	});

	return io;
}
