var crypto = require('crypto');
var async = require('async');
var mongoose = require('api/libs/mongoose')
var Schema = mongoose.Schema;
var path = require('path');
var util = require('util');
var http = require('http');
var AuthError = require('api/error').AuthError;

var schema = new Schema({
	firstName: {
		type: String,
		required: true
	},
	lastName: {
		type: String,
		required: true
	},
	email: {
		type: String,
		unique: true,
		required: true
	},
	username: {
		type: String,
		unique: true,
		required: true
	},
	hashedPassword: {
		type: String,
		required: true
	},
	salt: {
		type: String,
		required: true
	},
	created: {
		type: Date,
		default: Date.now
	}
});

schema.methods.encryptPassword = function(password) {
	return crypto.createHmac('sha1', this.salt).update(password).digest('hex');
}

schema.statics.authorize = function(username, password, callback) {
	var User = this;
	async.waterfall([
		function(callback) {
			User.findOne({'username': username}, callback)
		},
		function(user, callback) {
			if(user && user.checkPassword(password)) {
				callback(null, user);
			} else {
				callback(new AuthError('Invalid password or username'));
			}
		}
	], callback);
}

schema.virtual('password')
	.set(function(password) {
		this._plainPassword = password;
		this.salt = Math.random() + '';
		this.hashedPassword = this.encryptPassword(password);
	})
	.get(function() {
		return this._plainPassword;
	});

schema.methods.checkPassword = function(password) {
	return this.encryptPassword(password) === this.hashedPassword;
}

module.exports = mongoose.model('User', schema);


