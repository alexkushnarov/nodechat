var HttpError = require('api/error').HttpError;

module.exports = function(req, res, next) {
	if(!req.session.user) {
		return next(new HttpError(401, 'unauthorized'));
	}
	next();
}