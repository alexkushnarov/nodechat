var User = require('api/models/user');

module.exports = function(req, res, next) {
	req.user = res.locals.user = null;
	var userId = req.session.user;
	if(!userId) return next();

	User.findById(userId, function(err, user) {
		if(err) return next(err);

		req.user = res.locals.user = user || null;
		next();
	})
}