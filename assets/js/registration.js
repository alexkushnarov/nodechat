(function() {
	$('#login-form').on('submit', function() {
		var form = $(this);
		var fields = form.serializeArray();
		var dataToSend = {};
		$.each(fields, function(i, field) {
			var name = field.name;
			var value = field.value;
			dataToSend[name] = value;
		})
		$.post('/registration', dataToSend)
			.then(function(res) {
				setActiveAlert(200);
				window.location.pathname = '/chat';
			},
			function(err) {
				if(err.status == 403){
					setActiveAlert(403);
				} else {
					setActiveAlert(500)
				}
				
			}).always(function(){
				$('#login-form input').val('');
			});	
		function setActiveAlert(code){
			$('#login-form .200').css('display','none');
			$('#login-form .403').css('display','none');
			$('#login-form .500').css('display','none');
			$('#login-form .' + code).css('display','block');
		}
		return false;	
	})
}());