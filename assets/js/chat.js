(function(){
	var socket = io();
	var input = $('#message');
	var btn = $('#send-btn');
	input.prop('disabled', true);
	btn.prop('disabled', true);
	$("#chat-form").on('submit', function(){
		var message = $('input',this);
		socket.emit('message', message.val(), function(){
			addMessage('Me: ' + message.val(), null, 'orange');
			message.val('');
		});
		return false;
	})
	socket.on('message', function(user, message){
		addMessage(message, user);
	})
	socket.on('join', function(data){
		addMessage(data + ' has joined chat', null,'blue');
	})
	socket.on('leave', function(data){
		addMessage(data + ' has left chat', null,'yellow');
	})
	socket.on('connect', function(){
		addMessage('successfully connected', null,'#12902F');
		input.prop('disabled', false);
		btn.prop('disabled', false);
	})
	socket.on('disconnect', function(){
		addMessage('connection lost', null,'#901212');
		input.prop('disabled', true);
		btn.prop('disabled', true);
	})
	function addMessage(message, user, color){
		if(color) {
			var li = $("<li style='color: " + color + ";' ></li>").text(message); 
		} else if(message && user) {
			var li = $("<li></li>").text(user + ': ' +message); 
		} else {
			var li = $("<li></li>").text(message); 
		}
		$('#messages').prepend(li);
	}
}());